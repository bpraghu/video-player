import React from 'react';
import SearchBar from './components/SearchBar';
import YouTube from './api/YouTube';

class App extends React.Component {

    onSearchTermReceived = async (searchTerm) => {

        console.log(searchTerm);

        const response = await YouTube.get(
            "/search",
            {
                params : {
                    q : searchTerm
                }
            }
        ).then ((response) => {

            console.log(response);
            }
        ).catch ((error)=> {
            console.log (error);
        }
        );
    }

    render () {
        return (
            <div className="ui container" style={{marginTop : '10px'}}>
                <SearchBar callback={this.onSearchTermReceived} />
            </div>
        );
    }
}

export default App;