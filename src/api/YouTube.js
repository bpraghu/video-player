import AXIOS from 'axios';

const KEY = 'AIzaSyDhgfbTYXGINhLs2jsrijt_czf92VgfmYc';

export default AXIOS.create (
    {
        baseURL : 'https://www.googleapis.com/youtube/v3',
        params : {
            key : KEY,
            part : 'snippet',
            maxResults : 10,
            type : 'video'
        }

    }
); 
