import React from 'react';

class SearchBar extends React.Component {

    constructor (props) {
        super(props);
        this.state = { searchTerm : ''};
    }

    onSearchSubmit = (e) => {

        e.preventDefault();
        this.props.callback (this.state.searchTerm);
    }

    render () {
        return (
            <div className="ui segment">
                <form className="ui form" onSubmit= {this.onSearchSubmit}>
                    <div className="ui icon field  ">
                        <input type ="text" value={this.state.searchTerm} onChange= {(e) => this.setState ({searchTerm : e.target.value})} placeholder="Enter your search query ..." /> 
                    </div>
                </form>
            </div>
        );
    }
}

export default SearchBar;